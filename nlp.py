import csv, os, sys, pickle
import getopt
import pandas
from nltk.corpus import stopwords
from sklearn.cross_validation import train_test_split, StratifiedKFold
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from textblob import TextBlob

# Dataset
MESSAGES = pandas.read_csv("./SMSSpamCollection", sep="\t", quoting=csv.QUOTE_NONE, names=["label", "message"])

# Stop words
english = set(stopwords.words('english'))
tagalog = set(pandas.read_csv("./corpus-tagalog.csv", sep="\t", quoting=csv.QUOTE_NONE))
STOP = set(english | tagalog)


# Pre-processing
def split_tokens(message):
    return TextBlob(message,).words


def split_lemmas(message):
    words = TextBlob(message).lower().words
    return [word.lemma for word in words]


def split_stopwords(message):
    words = TextBlob(message).lower().words
    return [word.lemma for word in words if word not in STOP]


# Training
def train_multinomial_nb(messages):
    print("Training...")
    # split dataset for cross validation
    msg_train, msg_test, label_train, label_test = train_test_split(messages['message'], messages['label'],
                                                                    test_size=0.2)
    # create pipeline
    pipeline = Pipeline([('bow', CountVectorizer(analyzer=split_stopwords)), ('tfidf', TfidfTransformer()),
                         ('classifier', MultinomialNB())])
    # pipeline parameters to automatically explore and tune
    params = {
        'tfidf__use_idf': (True, False),
        'bow__analyzer': (split_lemmas, split_tokens, split_stopwords),
    }
    grid = GridSearchCV(
        pipeline,
        params,  # parameters to tune via cross validation
        refit=True,  # fit using all data, on the best detected classifier
        n_jobs=-1,
        scoring='accuracy',
        cv=StratifiedKFold(label_train, n_folds=5),
    )
    # train
    nb_detector = grid.fit(msg_train, label_train)
    predictions = nb_detector.predict(msg_test)

    # save model to pickle file
    file_name = 'spam_nb_model.pkl'
    with open(file_name, 'wb') as fout:
        pickle.dump(nb_detector, fout)


def train_svm(messages):
    # split dataset for cross validation
    msg_train, msg_test, label_train, label_test = train_test_split(messages['message'], messages['label'],
                                                                    test_size=0.2)
    # create pipeline
    pipeline = Pipeline(
        [('bow', CountVectorizer(analyzer=split_stopwords)), ('tfidf', TfidfTransformer()), ('classifier', SVC())])
    # pipeline parameters to automatically explore and tune
    params = [
        {'classifier__C': [1, 10, 100, 1000], 'classifier__kernel': ['linear']},
        {'classifier__C': [1, 10, 100, 1000], 'classifier__gamma': [0.001, 0.0001], 'classifier__kernel': ['rbf']},
    ]
    grid = GridSearchCV(
        pipeline,
        param_grid=params,  # tune via cross validation
        refit=True,  # fit using all data, on the best detected classifier
        n_jobs=-1,
        scoring='accuracy',
        cv=StratifiedKFold(label_train, n_folds=5),
    )
    # train
    svm_detector = grid.fit(msg_train, label_train)
    print(confusion_matrix(label_test, svm_detector.predict(msg_test)))
    print(classification_report(label_test, svm_detector.predict(msg_test)))
    # save model
    file_name = 'spam_svm_model.pkl'
    with open(file_name, 'wb') as fout:
        pickle.dump(svm_detector, fout)


def main(argv):
    # check if models exist, if not run training
    if (os.path.isfile('./spam_nb_model.pkl') == False):
        train_multinomial_nb(MESSAGES)

    if (os.path.isfile('./spam_svm_model.pkl') == False):
        train_svm(MESSAGES)

    try:
        opts, args = getopt.getopt(argv, "hm:", ["message="])
    except getopt.GetoptError:
        print('nlp.py -m <message string>\nnlp.py -rt <retrain data>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('nlp.py -m <message string>')
            sys.exit()
        elif opt in ("-m", "--message"):
            prediction = predict(arg)
    print('The message is...', prediction)


def predict(message):
    nb_detector = pickle.load(open('spam_nb_model.pkl', 'rb'))
    svm_detector = pickle.load(open('spam_svm_model.pkl', 'rb'))

    nb_predict = nb_detector.predict([message])[0]
    svm_predict = svm_detector.predict([message])[0]

    return '\n\tSupport Vector Machine:' + svm_predict + '\n\tNaive Bayes:' + nb_predict


if __name__ == "__main__":
    main(sys.argv[1:])
